/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createEmergency = /* GraphQL */ `
  mutation CreateEmergency(
    $input: CreateEmergencyInput!
    $condition: ModelEmergencyConditionInput
  ) {
    createEmergency(input: $input, condition: $condition) {
      id
      timestamp
      name
      description
      createdAt
      updatedAt
      owner
    }
  }
`;
export const updateEmergency = /* GraphQL */ `
  mutation UpdateEmergency(
    $input: UpdateEmergencyInput!
    $condition: ModelEmergencyConditionInput
  ) {
    updateEmergency(input: $input, condition: $condition) {
      id
      timestamp
      name
      description
      createdAt
      updatedAt
      owner
    }
  }
`;
export const deleteEmergency = /* GraphQL */ `
  mutation DeleteEmergency(
    $input: DeleteEmergencyInput!
    $condition: ModelEmergencyConditionInput
  ) {
    deleteEmergency(input: $input, condition: $condition) {
      id
      timestamp
      name
      description
      createdAt
      updatedAt
      owner
    }
  }
`;
