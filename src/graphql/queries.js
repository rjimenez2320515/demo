/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getEmergency = /* GraphQL */ `
  query GetEmergency($id: ID!) {
    getEmergency(id: $id) {
      id
      timestamp
      name
      description
      createdAt
      updatedAt
      owner
    }
  }
`;
export const listEmergencies = /* GraphQL */ `
  query ListEmergencies(
    $filter: ModelEmergencyFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listEmergencies(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        timestamp
        name
        description
        createdAt
        updatedAt
        owner
      }
      nextToken
    }
  }
`;
