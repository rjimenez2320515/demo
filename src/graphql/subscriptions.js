/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateEmergency = /* GraphQL */ `
  subscription OnCreateEmergency($owner: String) {
    onCreateEmergency(owner: $owner) {
      id
      timestamp
      name
      description
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onUpdateEmergency = /* GraphQL */ `
  subscription OnUpdateEmergency($owner: String) {
    onUpdateEmergency(owner: $owner) {
      id
      timestamp
      name
      description
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onDeleteEmergency = /* GraphQL */ `
  subscription OnDeleteEmergency($owner: String) {
    onDeleteEmergency(owner: $owner) {
      id
      timestamp
      name
      description
      createdAt
      updatedAt
      owner
    }
  }
`;
