import './App.css';
import {HashRouter, Route, Routes} from "react-router-dom";
import Portal from "./screens/portal/Portal";
import Notifications from "./screens/notifications/Notifications";

import { Amplify, Auth } from 'aws-amplify';
import awsconfig from './aws-exports';

Amplify.configure(awsconfig);

function App() {
  return (
      <HashRouter>
        <Routes>
          <Route exact path="/" element={<Notifications/>}/>
          <Route exact path="/portal" element={ <Portal/> }/>
        </Routes>
      </HashRouter>
  );
}

export default App;
