import './Notifications.css';

import {Auth, API, graphqlOperation} from "aws-amplify";
import * as queries from '../../graphql/queries';
import {useEffect, useState} from "react";

function Notifications() {

    const [notifications, setNotifications] = useState([]);

    useEffect(() => {
        const retrieveInfo = async () => {
            Auth.currentAuthenticatedUser()
                .then((user) => {
                    console.log(user);

                    Auth.signOut({global: true})
                        .then(async (r) => {
                            console.log("Saliendo de autenticación");

                            const allEmergencies = await API.graphql({
                                query: queries.listEmergencies,
                                authMode: 'AWS_IAM',
                            });
                            setNotifications(allEmergencies.data.listEmergencies.items);
                        })
                })
                .catch(async (err) => {
                    console.log("El usuario no esta autenticado");

                    const allEmergencies = await API.graphql({
                        query: queries.listEmergencies,
                        authMode: 'AWS_IAM',
                    });
                    setNotifications(allEmergencies.data.listEmergencies.items);
                })
        }

        retrieveInfo();
    }, []);

    return (
        <div className="App">
            <b>NOTIFICATIONS</b>

            <div>
                {
                    notifications.map(each => (
                        <>
                            <div key={each.id}>
                                <div>NOMBRE: {each.name}</div>
                                <div>DESCRIPCION: {each.description}</div>
                                <div>CREATED AT: {each.createdAt}</div>
                            </div>
                            <br/>
                        </>
                    ))
                }
            </div>

        </div>
    );
}

export default Notifications;
