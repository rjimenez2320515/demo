import './Portal.css'
import {useState} from "react";

import {withAuthenticator} from '@aws-amplify/ui-react';
import {Button, Container, Grid, TextField} from "@mui/material";
import '@aws-amplify/ui-react/styles.css';

import {API} from "aws-amplify";
import * as mutations from '../../graphql/mutations';


async function executeAPIMutation(timestamp, name, description) {
    const jsonAPIMutation = {
        timestamp: timestamp,
        name: name,
        description: description,
    };

    const resp = await API.graphql({
        query: mutations.createEmergency,
        variables: {input: jsonAPIMutation},
        authMode: 'AMAZON_COGNITO_USER_POOLS',
    });

    console.log(resp);
}

function Portal() {

    const [timestamp, setTimestamp] = useState();
    const [name, setName] = useState();
    const [description, setDescription] = useState();

    return (
        <div>
            <Container>

                <>
                    <img width={100} src={'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABXFBMVEXy8vLsO0UQarZ8fHzx8fH29vZ4eHh1dXXS0tJ6enrIyMjqPEXz8fL4+PgQarWrq6uioqLo6OiBgYG5ubn18e6xsbHb29u/v7/w8/Hv8/aGhoapqalwcHDx8+/y8fXm5ubrPEEAZrcAX6voKzcAX7AAXbJHh8CPj4/sOknxOEbpPUMAY7bX19eUlJT16ejr9PnnZG/F1eH8+/Tturn10tAAYqnrdYElcLPuKjcAV57oV2b/9+/T5vL57vXx3NbvOErnfofmR1HhQUPs+/XmLDCNstLppqnvxsXqnZjmhofka3LD4eemj5/sU2Dqpqqev9ZilMA1crAAW7pNWZx/rNDFSFnnipWgUnZzWIbs9+s1ZKK2S2aDVYDbQU5dk8v2MzSuyds0dqp1oMzWVlwtbqX32uPppK3vanZbibZHg8D20drnSEvwn6vnf39ekbXqs625ZYJQYJHbPleqS3SbBVS7AAAekUlEQVR4nO2dC1fbSJaAZeMyAZUcHgWUCBKS5SSSnViA7eDxKzEKz15eMwwz3TPphXSnJz2dTHZn//85e0vyo0qWwQYDZrfv6dMBYVv1+VbdV5WqJCSh/9siIWlISTwyGZ7w0cnvhI9ffid8/HJ3hDqmVNKoRCQJ6xJVVarquuqLS1VKMfydwt8leJl+Z624S0JNI1jV67quKxRJOJ9vFNvSyOeJhKii6/W6rmKsaXfWijvVISgMEdLY/ba3f1Bp2vZbxwrEeWvbzcrB/t633QYhSAXd3lkr7owQIQXYjr5WbCAqGyCyHONEZpcYrV35uldoEEWhd9OQERNigtmoQor73fZ/HH6wyo5hZGPA5jg+Fs/o+NeA0ylb8uG7o2IegSo1CcauhEfZqFESqq5LQBu145PT0h//VDZiA4vz3so2D3YaVPWIXsfeCBs1QkLskmpVWd/+NWWaKTP1w1Y5aw+MaNtZ6LRvD/eKqkdH22FHqEOyUTvbTJumGU+l/piK/9nZeju4FqHDOlkj6zgfzoueyhhHxTkCQiJB5ySKe3yZycXjgJeC/8fNvzjG4DrkQA2rspP3aJ1oo2EcAaErVRFdPykx5cW78ldjiIHICRimsv1m93s6Ih8yCh0i9+JFxoxv8nzx+A+GfSNER4beGrMrO9XRGJzbEGKV8UH3PM2koXfGUwJiqnkzQmZ2ANKwml/yCCek247I2xDqYBEUd3szE48S8283BGyJHLOae3kVuurt3OOtCKsbwJeLpyIJU38P/PyNCWXZKIMeddV9KEJc9aB/mqlowHjqT07YlrJQzXAGVC0Qxt4a75vftPq9E6oqIZJKqxeXGTMeCQgXzVTpg+z4VLEyBGZ2s3lYWWNSOWw2bdkqQ0xnGa3oTRbD1jZkVrYN698Fj8VzNw3mbkKoURc8RO3HP5h9OiizOpupzJ//ExCalTfn33aLn/JE2VDaQvL54m5h712lmbWMbNZwHPh/H1XGHOug4UGedcNY5yaEGEtV97iUM3sIU51/TTNj/uXgvFCEQNWjEFRTVcK0/X6V6iqiFFAbhb1/NGULIPt11pgTM+QvhLKw/L4Idbyx/jmTS/UbgfF4OrP5+Xi9riuI2Vus0Z4cV9M0WseQ9qvUI8WdN4eW1W98stxErhRUSm7UTYcmJERF6nbaBHcQRQhhaS59enbhqgqRKCtkYEwxiP/VBCIxNWKWaPl/13WMlPzulzUbeiwA2T1+1IbRKu/nXardgHF4HbrVWhCA9koJIrf05dl6ApJ7dahhAxkFzhe/VECTRlaO6LFOzKoUkX4DazMsoU6qx6V0pAUtmbnn5sd1usHKT0Qd6mMxU7BHteJ+04p0J7LsZJ0jShNDtnd4QuKepM0oHwHG5fnpsYvAj0C2oQ4HyHyBSvNEV7184adylNUBf5K1DvLD29OhCMFk1H6OimFgVKbTn9eVW9XMWh0QKcU3EJbGsrGwA4Ek+QP01CE76nCE1Yt03HcRIiR4d/OkplTJ7eKr1k0k6jXeyZGxjyzbO9qQ3WMYQkTPMhHjL5XKxU9qVeyqIykhUTCuuteAAWmE4xwYjMbW+ZCZ8cCENIHcExaFiilSinXQk3Vw6ZDnjyRnheS+DnpUPn3NBgFdF9B2WDXnIO/hISAHJlSr7oveNMk0NzO/XqARFRx4wZK+u5Y1Yr0Bq1VpoLaHHUAGJcRVZmN6e2g6d0wRGXF5LLij7n1fgK7qxMKMVrOIWKgw2OcMSIhR7TQX4QQzJ99VdVe/i3kHFut4ZB8i1jChYTSL9RETqmS9FMFnbl4oeUkjI6v8cQLBHsRoulKssIB1i3eRMBrtXQph6kCMAxGqVQAMG1BmYdzq7TAGEEz2IZJztkJ6tAuEDlbfGIQQgwbNXhtTulBuFu0PJZpLC82e5ArC1IICkfsAHzAAYQKts1poyM1nXtQUrA/pfW8gdUq8/MH7kPuXszF5t04H6afXE6rVWikUxQBu6cyFRHa0s0TRgjX4HveMkBYh/7CLaBC/eD1htbbZMwbN9DGqkhE0fzDBulb4YIQnCZxmcZBuej2hC24iPAZ/Xof8dhRB6GBC6ypqHIa9RtZoNgYoi19L6L7oBbysEbAx96dDCdM6rR+UQzrMWpUBsqmrCcEpnYRCNTOVfuGSu1xaEN0S3ct/7UmOrQPt2mTqakJaPeuJRZ+fuJilufcr1KXaxvn7cBnd2teui/avIbyAbEK0MumPVCOJu1w8ES16XXfJXriK7mztXNdPryQktXQoVks9/6gRVgQcYdsHFE/FOtqzZEfI/I3Yrnq1V+xPSCTi/hz2g+mPLsEPgOcLgNT3bHEsOrGt/NVGoT9hlVZPcmJVGzToUe0uUqWBhFVYQYuC4485zgHSv7/iXf0J1Y3jtNhDIdSm9+giegWUqGrnPCKrh1t7Vy43iiZUNejztVKcr1mU4uYLFxKawbPr0Qvc21XevefGoh8FFN0rlsb11SGhl4IKTXD07n16+WjRKQbXLxibrPVTXmVZarREExKXbmyHXf3PLJK5bz8YFkgnvPqhEIbLhrWv6vV+SuxDCDlvOhStpdfJ8LXskYumUR01mrw9zcqOXICwrs87+vRS3X3B57zAWjpWHryLMmFLb1HBkrnCBlibSr6vC+tjaaqhPmpmztBYADLRVO3IyvKphpM971sKjyYEO5rjFRjPvbiHkszAUle1N0JxSjacRj8lRhNWfzRLcT6eKYGVccdGiRTr+aYlpIvv1/qlir2ERFerF6FFCOkLRbrtyp2RSt3djYnLjq1vENpE6aCXUFWr7s+mMIVtnoyHlekKxd651U2lZNmJfdDqkRFXRC9VyXHIFZ6O0yD0BVOXVAyH1yIEbzjKJ0YQaqBCToMQjl5UAfvOWz2U6JpezHLTb7Js2PnI2YUIQmVbnCXMnCh33uCbiAIxOJcR29a5FzXLH2Fp3E0xry/Vxq2PBlInTX6R7pbzoREV1/QSKtviJFPmeMw6aFtUWshyPjH79v1+VGcLEYLTcze7jgJ8hvli3IZgR/TvD2JyN7SxDbuBeh+h6tGhForX0hdj5OoFgWC7KHe7qSzLMBJ7Z1JChG7VPRUD0hNEHqouc41QXUf73ZHIFnA2G70zGeFeii4EFaZSNY3ewSz9SAQcfOPfXY/BFhV/8Xr6W4hQoy9EOzqmnsIXKiXQnrAkRW72LigO63A9zWswbtbG+yFTvSHUiG2rcM04xMq/hMne9En1oZP6q0X3znlC2Vm71tKUhEV56RoeTzvaFrVOeMKsbX0Kv4QjxJqLjnObnS5qlswflbGLuUXRdHpudIuLb43yni6J86YcoU6JctkN2GAQZtarY+vuA/ke00bZkNsPyTlZ58OGJBobXodutZbpdtJUKnep4Dt9zPr2oiUk5cDJdh4DlLPlXSSmGCLhmdkN2VKp58ea+2DTMIMJJL1ol1vE6MjWu1CNnyOETrrZNTLxVK50fzP1txFIMXif+FtdDMJ4W6oJzjCe+7hxz229mdA9PsWIWQUkKJHX4caZ6AzXx9vKtEVtCI8vWF/dfraUWdKuIY3HL2/ydMMDiEr+wRMaTdqvl5KasHots70x3lamLRgdvecJ7WI/Qk2cEU3X6OMgrHufeEuTtb4IgRr3y8YJDxj/VVIfRy/VMD7gqxnWmmA/OEL31OTKF7mzMc6bQoJ/KW+9zXYHolCR6hKyNZbdp+vimXX27NL9t3Z4wZR+cuQuoW3s8t2UI4Rh2NFhyTx1h3126aEECHHFsduEhm19UbjCaZdQOeEepiiZn9UHn+8dUCjGyrkldwnLB4izIB1CNdEpQbGHeHPjWiWNFFSw+Meqm3naXe7WJsRqrcQHpfFHEtAEooK/4AiNIuohVMUiW8r8eUyLpNGikwofm1o7ejcea+uQKNvcIzGp3OdxmzG8UnTvjUC4zwXfLUKikhOTJzx7HHlFS7B7JCyxWZO6i0+64/BSCEovHpMKYZTt8jo0mtzik844rPHZbzxee1SEVM9vcfmFEfukhsehWq0JYfem+0jcfSBU9/hlUoa16/YSXgiEl97jIsQenyM6xi9upxrVIRQm1cZv8cU1guk5T2jtdfXTItSVM/75ydw2EWsdCCmKwnblbf8qbNXbvSRxL+DfHfU7fynqWv+7iG/1AdUdoVaz3+6kfFTKL0WEmI1fm6KsTi/MzMwsLC0mWEqFFp8IAp+SaP0YRLz+Cxbb755t/W21fdPO2xdnlVYLVtuXXq0mWtfCd0lwr3ryyn9rJ74Gwl1u6bBtHKBw9p5An3lnkb4g3WI3Wp2ZmEwmk1OTk8nlhVm48GxlkpOVBJJWp4Ifp/3WKTMrkytPW41XFoJXrzxrJZzsr4Ek515P+9pAC51PnIJbIPFlvkzMSmi6e2VieeZJV5GY6kWB8LC3kq1emsKMTLeXolcTyYmOrLxCkvKUuzAxMcn0NOf/OPU6YIAXJBfa9389FfztZbtHzHTfPjX5mvGgef7SxBOfemaKv8tyAggnuQvw1tWON9f0PFfJkK3fep+MFJeYAGFnqM5OsDsl2Vc+FTQzIEy2ZaVLODHp35QnRLOsWX5jV3lC1inYxeTybJvQV6v/NS4y6hn+LpNzbcJk52XJuTYi1ur1Jk9o5cOAWE3zz8Gm1E4138eZmliYfjI9PzPhd8OAcKEtT3nCBQSDQ3nGEUKzppZfs78FXbhFCG+cmfMZZlBAOPdqcfHJgq+eZaVFONe5y0KLMDk/PT299HQ56b+uPRKJqnCEYEwbYWeHXWEdVErpPDCVeMm+6lVmSJGSmJ5tQU/NSUrLqiktQgY5NZcI6xB+nnyl+CgKR6iAdW6pbrWlwwT7sFX2MUlQok+4jJSu8fQJJxfZFQUtsWZNLrXGNsHKoeAuimFCtSYQbm6obURfO6/bVSnUUeucsMIKXjX1mg2cSTaG0MJUl3AOrs4q8Lf2N94i9H9kYxR0GxDO+pcWkqzlbUL+Li3CVk9gv7Q/EhJeWhHcRTFsaZBIeLrRiet8wmXB/fQhfPmE3ZN9GRwhWp1k9gctdRvHEQZtnucJ25euI5SU5SnuN93jK4oQtoUJyfpzfj3prxtqx9S8ZL3mWYKrLUYTTixLTF0rYBt5QkBLPkVocTLQTIjwiT+yBMKlQQnnk52PBFuqvRMmLwrhFRYhwhddQrTgG7DlpY53bhFKwcZkqEvoX0/OK/6wahGyfsh6LpoMbEqIcCloJd9L2V+ZUQoI+buIhMG381TpEFo3JGx5i6nkxMyrFk5gS5/68iwwkL6lURZ9fyUQomTgJpSXHb3z45D1NPANbUsjIWXR9wOzbVv6jLtLiHCVDYrgS8MU34IQLU4F7jg5+fKV0iVs+amFDiE0+nVga7qE6NVk4EP9rhC0rm1LwTw+Y0p/mWjZ0tXZ1cV53+c8bXsL+GZ9lzvfSyjNrnS6xe0IwWfPJAPGqZWFtkPvBDRLHGHgsF4rrPO1CMEy+j3J71OT813Cpfn5p3NM55OvUNvjr6z4rjy5zJwq4kKfyQgdSomVEemQtWlxYTkII3yP3tIhH4m2dOj/k1xVmHkJvoyXwTAMvvGp13480ApW/E+c8r8iPmqbmHydCKI27i5P+uhwVITw4YknwVfKfHFAOL/EZD4IxRjaFPKDGdBUl3AWXjg36xsLFjlMsbZzcenU5Ny0n63MBzT+/5damZI/Dtt3iRiHi9w47LGlu2FCVHvOucP4abVnZg2hJ76CFlDbW/B5mk/I7spCvGXUJvT75tyML8sTrXggIJyaSk7NvV4K8oggaptfmp/0CaUO4bLE3SXKli60CLF2IJYxwv4wHNNELWdTAo+O+vnDCXZbpqnkq+kWoa/Tlk3y4/cFpU04u7q6Ottp+3zwnfmKbn3ytf5wYaoT62JN2lgTdNgT04TjUq5a2rlHAq0E+VE/wkTLXydnnrR1uCxkQGBWUdeW8m33CWe7Ac0ghMv+mPd/0zWqVETCnshbTQuE/OeutsIZ5Jvn/jpMMsJZhjTXsqVolf220hI/VE4I/lAg9D2+H74nBiAMelQr52SEH67OLYgq5IelWrXrDlcWEkFmEZgR1MotEnzJxB+HflrBXjT1MojamEYhCWl9kB+swEC8gjCB/JG4oEihcRjOLSAHCbxI8DFgaYT80LB6N8xQT/tkwHCjZPLZ9Opsy5iutnW41JEER+hbOBYF+SYJMop2zhRQMN94pQ5n/YbPtjNg4S6Bu302vbg6uzg/xUzzy5aKwzm+YfcQ0lCdZr0Tli628uqVSb+XzSuS4A+Zs2rrMIgrl4MhxwhnlzshQfuTlq8hRH6Y86wT03Tu0h6l/pUgMpiaaI9JrKmhOk3vA8vohK8mZi66hMuTHWsxNdmKaQT70a7TJLu5QUDoMyVftW/hW6PJVaT4BZgQ4WSLUFpMtgxIVJ1mRbjvcsf599baelYc9q+XosTSS1aigQBxZTmwzeFaG1xktbaVwPknWqU1SJlYBW2lYzPR6yAGCopoIUK4FPQB9Jq9aQb11NqgQy6+noOmQFugMZMTT2e7tTZP/yaENPu9pbYrat4osbj0jJVLXyVa3uvVtCAMi/3b8mNPgquLwU9PuoSvuMvTQsyBFjvvD36c7r1LAuwQml2cnmdtmVmYnuU+AVNF2E3C2OuJWMLzFi+EeYtwnblPNbrn1ZF17Z7LnT+Jr4q6C7uoBLZU+AAgPBDmLY7cXkJhGj++6Y7tIv2oJ/CoLhSihLmnlqh5cf6wVHtcc0/gLLidJAyj0bMLGMUuPwecSl/c+05QtxK9aPGEzXyPfiD7EObxze3HNY+vfuM3AjPWSO8Ds0Tj12KUcp8fz7o9JvQdv6W79U7v6aU6ofx6Gn9Z24M09WaiErZdbZfwSO/ZAkzFKj+Rn2Km5kHaekP51BS2rNntfWiSSjrljGkqlTsOlwHGWdCuleUOl4K4O+qxUOWku8zbLOXYg4ePQ42exDZYiLV7qWFbB9HaQduleHetvr+w7XE8UQIRDVkz3nKE59FmEq1Dmt/NEdNsjfA9N/WGQqVPse4aYcfuLSW2xN3srqA1U5mz+9hDdxQCNuSozBEadqPPKzd+5J+uNH8lj2StvqTr/MyaXa70i8fIdlp8fvTabSXHQ+rep984TyFb5/3cQGjP7sx29XEQqrTAPzOTfVvsR4jVn7nDfiBHHPachQcSFb0Rngpq9u17oUpGPFN7HPkFCj279sbrS+j7i66peSxPzWhHsvj8od7vQRgdq5v8/lfm5uPw+KRi8dsO2KTezwcQtfrRbEdubI/y52O7rUlHdEpQ0T8mKrCjTsx446n9jqJjmyVmhDT4szLg+QMPJppEvXdO5zlgtoNLAV1hIInC1fbBc2RqY1uPaomGad6KyZ1euhWzN3DfB9JUyVW2c6UuYck82Rjzborr3rmR7eypYGfL55T0taWSv/kzZ2tSENdcpfIxEKIT/uQLw37/z2sarH02hRmaj2PeTTXvF95TGEblukcmyUVasDWpMd+fRs0Lm7XG3h9dSyiUTdkxFuNbVcRsP50j4Vxl47f8dbZfQ8fCxqWbmzASx3SfKLbpVb4i7BhhnffuExUStypU91Mp86Myrqm+hnV1Tzy6zG7o151bRGl418T0+rgGNhpWP8Eo5Le/PNfq12V8xEU1ITiFwMYd01148lTbt7j9PQ1DLqJBjn5TxP0/4pmLcfUYtCiaGevrYGaRbY/B99PcKR1PQpVUysK+3nZxQJMIibCweig9pnmi8sUSj7r4OuhGHv4eJyluI5c/rI/d3omU1lFjq5tUODJEbI0BD/DEqrL9nN8QK546VcmYWRta9zYOsrFuxCbHrH1lwEwPu4RN0vCIuTNlzHamw7q3Y8W4TSFjcjMv9d1bP/RmjNHFH7rjEMxO6vl677TxAwqVdLdoG4Zjc77iyKsPdNJcIMpnzpwyJW66en5s9skg3zM7ym8T4WTfHw5XGAw995wy0/+iVXdciotavY7fZYVDHw2r2K84Ey1EEU6yYqc5biM6LoQwCAtGTDgVKfvuqsQ+QsKb7TLEi+q4FKWoXpQNpztXIctOM4+Gax1xqxel0Onwm7WxIARDqOaFFc8QfDvftAHtaFuIK21APxVCG/PSJQ+/7RCmlJ2ix5eAZcP4ivUhCVsHdgmELMuQxoBQqn8ti4c9Gh8a7tAxFyiLrIvnPaXMzImCITB60Izf05W99/yZCPBjtkBv1Co1fB5SKp4+q2J2NNjoGz6w1JUj4VgykPL5lWd29Rd2EHdOqC3GzfT2Q6cZ6jdxDMYc56eIRWwDCVH9mk3o0CdAfMipDFSwjS2BMLb16caVMpWg78QDHlNmChCx/iDxG8aerhZioXPIjYgdvIcR5TgTOswZEMmDrJUCN1FXvoXPsDSMvdtZBbLxMR0PSfpBUilmLJUdKxs6Mdc4yN9u60Oqq585RP9oZ5OdYynd+9lImNb3LDsMeHj7rA65lzlT3EIqnvkMiNo9rrYhGlbV+v57Ec/JOs3G7adVEhu1zVToPFnTvKxV6fBhxI3l+3pdzx+UQyflglXtu3JmGMHV79IiIAvgNteVezyXW6p7xUrZCHXRLacwkjNtqbRxkUkJ5wmwYC69TcFn3AckBe+ECrJhC4NQdmSr4I4muiKa0pNJsT33T9wqVu8+KaZ13dXeGaHjqgFWPqqPKH7UJar486ahwZg+XVfI3YeodQ96qBwLjUEnZu0gOkTl6SohOrj441J4LMbNXG77HoJwvLFjO1vOb7GQEhngiDyWhjHBoMWU6DRYt81d1jaoSu5kNGKsaUTSaePAMmRHNDJvs8YRomSU5/YSCcZiPBTAMcw/nNGqejeH6LLHB1TyZSsbC41BsKL2zqjGYPtmYDU31jdzYT4W4JxeqHfjN6juouJa2ZGzoTEYc7bAio42E2cZP63WxCOQ/Wp4PJVLn9TuYgtCTNXifqyczRqxULhtfChCLErvIIkj7ovgcFKzQ+ibH7N05m5gnW3SN4qvVdc0zKbo83tNx/AnsfmShWxYh5+ojvGozIwghJ6EfX9gVnObZ+73eVeVRuEeoel13csfVaxQKhgo0HhD7nCJD9G20/FexJyZKqXO3GqVjGJBKqGel9/5YBlGmJB5RWsvf6erXqssvOk1qazDlkofaxu3Ho9gQJCX//KbBVGoHFahHDO2ChrEoneauCm1F5lQeNPWqZn5vK4SSdV1liEP97CGB5ZFy0Oo65HGOydcTfO7J/CWDz+pd17MJFX3jE2Ch3tqnD20aGZOt2ta4DyG/J7BcuiepuV3frKcqPHHRqBx3nv23+hFdYlycZqL6qn+itRM6cfjGlHQsIQAqOcLb+wYjD857AB9K2p9KCjDlu5vJFjXkfsxHWgxlFOx7guJFYN0FY1o1C8f+0VkqvsW3oeBj/DzOv86xfAS5JFPv7xpWu8Nx94Kl9PYOgTDcb42VHpfJ9+oErrY9J8giuqs/ogsXX68qEkI6XWdEZAgdqXdWivEnNRj3P7Q2z0/iIVT+A6dHJONbHMH9zxif3dCWD38zPcbPRlHBzKXTv/X12/FvKIgoNB1pkzVFzb1QeEa0pGi5IuFd2tNMJ1GNhKQzUvE3lvvGnSQ9VyjI3RhNK5fhs625pUYT22aPzTLZcs+PNj7Vmzk2U7LHkJU0xD2PEXB+UaxsPfm0DbKlpHNxt6+fRtNCGId7npSvX6PU0IQhxOXVJXjUs6M7qd+avXfkPPIWcOyADRmf6isvdkP5M1a5bAps8vAlpXBtTuxXu/XNqFWc0/T/b5+/6Kq28AYjx6LcfPvjjBTC2IFwn7sqzCBzsga9n7eewC2QAhRah9LuXB9o63Hv/UmdkOKYdlfGwodbpXFKAW7tMoYM1GEZurfTh/jMahY8tcipKb34eb7CfgADHo8SzHGVCgK+OHtzVQIA9I2snLMsveLmsYG4IOvkADG7dO0Gd9M8XWO1F/tmxE68lbWdqzfziNPSn8QUTFx3YsXmT/mUnyt6n+2nD7m8Vodlt9XjvIevnbN9r0J5BKEoNpZ6XnX6qTify7fCDBmlO39fyLvgfzDVYIgJD8ppU0zyPpLQ/VRw3CY7wR/Yh8UyIiq9SMXtVrdcC9ONjMsLDf/NBShbctA5zTfFIjneQ9uWqIF0l7Vlajirp+9MDO5v0elsX2lbFmxyvmu5rFYfSTlnjsSomksDlBrFx//yzb84CVQJYvJZO5JTxB/Lon9HeDstfNCA0JWCND9ZOuhOa4XiiC03j1iWQNg+qROLCtnA2GUVjuIa669O9pt1JH3CLBCwrJ2km/s7ux9XftgO+2gtB2byh/Wvu4BW55AFkXx+FnOq4VlgARyfEhwXRdhCechWdrdLQSyuwvpVB5jRD0P0kXqaUTDDxmb3UxYXRprvnIoVeE/XdeD7QF1ltjDBRWzxaJsbguG35g/M/67/C6/y+/y/1xQeKvXa+Sh2zu0/C8cAOjlIfPnRwAAAABJRU5ErkJggg=='}/>
                    <h1>Portal Web de Emergencias</h1>
                </>

                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <TextField fullWidth id="timestamp" label="Timestamp" variant="outlined" onChange={(e) => {
                            setTimestamp(e.target.value)
                        }}/>
                    </Grid>

                    <Grid item xs={12}>
                        <TextField fullWidth id="name" label="Name" variant="outlined" onChange={(e) => {
                            setName(e.target.value)
                        }}/>
                    </Grid>

                    <Grid item xs={12}>
                        <TextField multiline rows={8} fullWidth id="description" label="Description" variant="outlined" onChange={(e) => {
                            setDescription(e.target.value)
                        }}/>
                    </Grid>

                    <Grid item xs={12}>
                        <Button size="large" fullWidth variant="contained" onClick={() => {
                            executeAPIMutation(timestamp, name, description)
                        }}>
                            Publicar
                        </Button>
                    </Grid>
                </Grid>

            </Container>

        </div>
    );
}

export default withAuthenticator(Portal);
// export default Portal;
